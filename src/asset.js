// import favicon
require("../public/favicon.ico");

// import logo
require("../public/assets/logo/danamart.png");
require("../public/assets/logo/danamart-head.png");
require("../public/assets/logo/ojk.png");
require("../public/assets/logo/qr-code.svg");
require("../public/assets/logo/qr-code-light.svg");
require("../public/assets/logo/bank-bri.svg");
require("../public/assets/logo/bank-bni.svg");
require("../public/assets/logo/bank-permata.svg");
require("../public/assets/logo/bank-mandiri.svg");
require("../public/assets/logo/bank-danamon.svg");
require("../public/assets/logo/bank-cimb.svg");
require("../public/assets/logo/bank-hana.svg");
require("../public/assets/logo/bank-maybank.svg");
require("../public/assets/logo/iso.png");
require("../public/assets/logo/aludi.png");
require("../public/assets/logo/ig.png");
require("../public/assets/logo/fb.png");
require("../public/assets/logo/linkedin.png");
require("../public/assets/logo/yt.png");
require("../public/assets/logo/tiktok.png");
require("../public/assets/logo/yukinvest.png");

// import image
require("../public/assets/images/hero-2.svg");
require("../public/assets/images/example-project.png");
require("../public/assets/images/example-project2.png");
require("../public/assets/images/home.png");

// import image/avatars
require("../public/assets/images/avatars/testi-1.png");
require("../public/assets/images/avatars/testi-2.png");
require("../public/assets/images/avatars/testi-3.png");
require("../public/assets/images/avatars/testi-4.png");
require("../public/assets/images/avatars/testi-5.png");

// import videos
// require("../public/assets/videos/flow-home-banner.webm");
