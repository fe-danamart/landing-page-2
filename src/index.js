require("./asset");
import css from "./styles/style.css";

import jQuery from "jquery";

import Navbar from "./components/Navbar";

// if (document.querySelector("#navbar")) {
//   document.querySelector("#navbar").innerHTML = Navbar();
// }

document.addEventListener("DOMContentLoaded", function () {
  if (document.querySelector("#first-loader")) {
    setTimeout(() => {
      document.querySelector("#first-loader").remove();
    }, 500);
  }
});
