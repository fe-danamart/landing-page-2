const Navbar = () => {
  return `
  <ul class="flex justify-between items-center">
    <li class="flex-none">
      <a href="http://danamart.id/" class="Consider_Home_LP">
        <img src="./static/danamart-head.png" class="object-contain" alt="danamart.png" width="150" />
      </a>
    </li>
    <li class="flex-none">
      <a href="https://www.ojk.go.id/id/berita-dan-kegiatan/pengumuman/Pages/Pemberian-Izin-Usaha-Sebagai-Penyelenggara-Penawaran-Efek-Melalui-Layanan-Urun-Dana-Berbasis-TI-PT-Dana-Aguna-Nusantara.aspx" target="_blank" class="Consider_OJK_LP">
        <img src="./static/ojk.png" class="object-contain" alt="ojk.png" id="logo-ojk" />
      </a>
    </li>
  </ul>
  `;
};

export default Navbar;
