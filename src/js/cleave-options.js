document.addEventListener("DOMContentLoaded", () => {
  // phone number
  if (document.querySelector(".cleave-phone")) {
    let cleave_phone = document.querySelectorAll(".cleave-phone");
    for (let i = 0; i < cleave_phone.length; i++) {
      new Cleave(cleave_phone[i], {
        phone: true,
      });
    }
  }

  // format thousand
  if (document.querySelector(".format-thousand")) {
    let format_thousand = document.querySelectorAll(".format-thousand");
    for (let i = 0; i < format_thousand.length; i++) {
      new Cleave(format_thousand[i], {
        numeral: true,
        delimiter: ".",
        numeralDecimalMark: ",",
        numeralThousandsGroupStyle: "thousand",
      });
    }
  }

  // prefix RP
  if (document.querySelector(".prefix-rp")) {
    let prefix_rp = document.querySelectorAll(".prefix-rp");
    for (let i = 0; i < prefix_rp.length; i++) {
      new Cleave(prefix_rp[i], {
        prefix: "Rp",
        numeral: true,
        delimiter: ".",
        numeralDecimalMark: ",",
        numeralThousandsGroupStyle: "thousand",
      });
    }
  }

  // only number
  if (document.querySelector(".only-number")) {
    let only_number = document.querySelectorAll(".only-number");
    for (let i = 0; i < only_number.length; i++) {
      new Cleave(only_number[i], {
        numeral: true,
        delimiter: "",
        noImmediatePrefix: true,
      });
    }
  }

  // credit card
  if (document.querySelector(".credit-card")) {
    let credit_card = document.querySelectorAll(".credit-card");
    for (let i = 0; i < credit_card.length; i++) {
      new Cleave(credit_card[i], {
        creditCard: true,
        creditCardStrictMode: true,
      });
    }
  }

  // npwp format
  if (document.querySelector(".npwp-format")) {
    let npwp_format = document.querySelectorAll(".npwp-format");
    for (let i = 0; i < npwp_format.length; i++) {
      new Cleave(npwp_format[i], {
        numericOnly: true,
        delimiters: [".", ".", ".", "-", "."],
        blocks: [2, 3, 3, 1, 3, 3],
        uppercase: true,
      });
    }
  }
});
