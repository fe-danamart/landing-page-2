let prod;

if (window.location.hostname === "danamart.id") {
  prod = true; // prod
} else {
  prod = false; // dev
}

const base = {
  endpoint: () => {
    if (prod) {
      return "https://danamart.id/dm-scf-api/public/";
    } else {
      return "https://dev.danamart.id/development/dm-scf-api/public/";
    }
  },
  asset: () => {
    if (prod) {
      return "https://danamart.id/dm-scf-api/writable/uploads/";
    } else {
      return "https://dev.danamart.id/development/dm-scf-api/writable/uploads/";
    }
  },
  url: () => {
    if (prod) {
      return "https://scf.danamart.id/webapp/";
    } else {
      return "https://dev.danamart.id/development/webapp/";
    }
  },
};

let CryptoJS = require("crypto-js");
const Crypto = {
  encrypt: (message) => {
    let encrypted = CryptoJS.AES.encrypt(message, token()).toString();
    return encrypted;
  },
  decrypt: (encryptedMessage) => {
    let decrypted = CryptoJS.AES.decrypt(encryptedMessage, token()).toString(CryptoJS.enc.Utf8);
    return decrypted;
  },
  decryptResponse: (res = null) => {
    if (!res) {
      console.log("data tidak ada!");
      return false;
    }
    let CryptoJSAesJson = {
      stringify: function (cipherParams) {
        let j = { ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64) };
        if (cipherParams.iv) j.iv = cipherParams.iv.toString();
        if (cipherParams.salt) j.s = cipherParams.salt.toString();
        return JSON.stringify(j);
      },
      parse: function (jsonStr) {
        let j = JSON.parse(jsonStr);
        let cipherParams = CryptoJS.lib.CipherParams.create({ ciphertext: CryptoJS.enc.Base64.parse(j.ct) });
        if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
        if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
        return cipherParams;
      },
    };

    let decodedEncryptedData = atob(res);
    let secretKey = "08Dana5714M81adMart79salji90";

    let decrypted = JSON.parse(CryptoJS.AES.decrypt(decodedEncryptedData, secretKey, { format: CryptoJSAesJson }).toString(CryptoJS.enc.Utf8));
    return JSON.parse(decrypted);
  },
};

function toLocaleIndoTime(date = null) {
  const event = new Date(date);
  const options = { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric" };
  return event.toLocaleString("id-ID", options);
}

function scrollToElement(element = null, paddingTop = 100) {
  const y = document.querySelector(element).getBoundingClientRect().top + window.scrollY;
  window.scroll({
    top: y - paddingTop,
    behavior: "smooth",
  });
}

const IncrementCountAnimate = (counter = null, number) => {
  counter.textContent = 0;

  const updateCounter = () => {
    const target = +number;
    const c = +counter.innerText.replaceAll("Rp", "").replaceAll(".", "");

    const increment = target / 100;

    if (c < target) {
      counter.innerText = `Rp${Math.ceil(c + increment).toLocaleString("id", "ID")}`;
      setTimeout(updateCounter, 1);
    } else {
      counter.innerText = "Rp" + target.toLocaleString("id", "ID");
    }
  };

  updateCounter();
};

const copyToClipboard = (text) => {
  if (navigator.clipboard) {
    navigator.clipboard
      .writeText(text)
      .then(function () {
        // console.log("Teks telah disalin ke clipboard.");
      })
      .catch(function (err) {
        console.error("Gagal menyalin teks ke clipboard:", err);
      });
  } else {
    console.error("Clipboard API tidak didukung di peramban ini.");
  }
};

export { Crypto, toLocaleIndoTime, base, scrollToElement, IncrementCountAnimate, copyToClipboard };
