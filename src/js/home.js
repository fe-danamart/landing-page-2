require("../index");

import AOS from "aos";
import Splide from "@splidejs/splide";
import ApexCharts from "apexcharts";
import { base, scrollToElement, IncrementCountAnimate, copyToClipboard } from "./custom-tools";

// ========================== START::OPTIONS ==========================

// START:: get data
const ListProjectData = new Promise(async (resolve, reject) => {
  try {
    const response = await fetch(base.endpoint() + "page", {
      method: "GET",
      headers: { "Content-type": "application/json;charset=UTF-8" },
    });
    const data = await response.json();

    // const dataWithoutCo = data.dataProspektus.filter((item) => item.is_co.includes("0"));
    // const dataWithoutFinal = dataWithoutCo.filter((item) => item.batas_tanggal !== `<span style='color:red;'>Pembelian Selesai</span>`);

    resolve(data.dataProspektus);
  } catch (error) {
    reject(error);
  }
});
// END:: get data

const slideProject = new Splide(".splide", {
  perPage: 3,
  perMove: 1,
  loop: false,
  padding: "2rem",
  breakpoints: {
    640: {
      perPage: 1,
      padding: "0rem",
      drag: true,
    },
    768: {
      perPage: 2,
      padding: "0rem",
      drag: "free",
    },
  },
});

// ListProjectData.then((data) => {
//   const ctaInvestProject = document.querySelector("#cta-invest-project");

//   if (data.length) {
//     ctaInvestProject.href = `./register.html?offer=${data[0].pinjaman_id}`;

//     slideProject.on("mounted move", function (i) {
//       ctaInvestProject.href = `./register.html?offer=${data[i].pinjaman_id}`;
//     });
//   } else {
//     ctaInvestProject.classList.add("hidden");
//   }
// }).catch((error) => {
//   console.log(error);
// });
slideProject.mount();

let listProject = document.querySelector("#list-project");

// set list project
async function logProject() {
  ListProjectData.then((data) => {
    let list_data = "";
    if (data.length) {
      data.slice(0, 5).forEach((item, i) => {
        let companyName = item.nama_perusahaan.split(" ");
        let namaPerusahaan = companyName.length > 4 ? companyName.slice(0, 4).join(" ") + "..." : item.nama_perusahaan;

        list_data += `
        <li class="splide__slide shadow-lg hover:shadow-2xl transition-all duration-300">
        <div class="item relative">
          <div class="relative">
            <img src="${item.gbr_sektor_usaha}" alt="example-project.png" class="block mx-auto object-contain rounded-lg w-full aspect-video" />
            <div class="absolute left-0 right-0 bottom-0 top-0 flex flex-col">
              <div class="badge badge-accent text-white p-3 rounded-none rounded-br-lg rounded-tl-lg font-semibold text-xs">${item.isPrelisting}</div>
              <div class="mt-auto pt-1 pb-3 bg-gradient-to-r from-sky-700  to-sky-700 opacity-60 text-xs md:text-sm font-semibold text-white">
                <div class="flex justify-between px-3">
                  <div>
                    <i class="fa-solid fa-trophy"></i>
                    <span class="text-xs">${item.jml_pinjaman_terbit}</span>
                  </div>
                  <span class="text-xs">${item.progress}</span>
                </div>
              </div>
              <div class="flex-none absolute -bottom-3 w-full">
                <input type="range" min="0" max="100" value="${item.progress.replace("%", "")}" class="range range-xs range-accent rounded-none" disabled />
              </div>
            </div>
          </div>
          <div class="flex justify-between m-4 text-mxs">
            <span class="whitespace-nowrap">
              <i class="fa-solid fa-clock opacity-60"></i>
              ${item.isPrelisting === "PreListing" ? "PreListing: " + item.days.prelistingDays : "Listing: " + item.days.listingDays}
            </span>
            <span class="whitespace-nowrap">
              <i class="fa-solid fa-user opacity-60"></i>
              ${item.total_pemodal}
            </span>
          </div>
          <div class="flex flex-col m-4">
            <div class="relative h-14">
              <h4 class="absolute font-bold w-11/12 text-base md:text-xl mb-5">${namaPerusahaan}</h4>
            </div>
            <div class="grid grid-cols-6 gap-4 mb-8">
              <div class="col-start-1 col-span-4">
                <small>Jenis Efek</small>
                <p class="text-xs md:text-sm">${item.jenisEfek}</p>
              </div>
              <div class="col-start-1 col-end-3">
                <small>${item.jenisEfek === "Obligasi" ? "Kupon" : "Intensitas Dividen"}</small>
                <p class="text-xs md:text-sm">${item.kupon}</p>
              </div>
              <div class="col-start-4 col-span-2">
                <small>${item.jenisEfek === "Obligasi" ? "Tenor" : "Peresentase Dividen"}</small>
                <p class="text-xs md:text-sm">${item.tenor}</p>
              </div>
            </div>
            <div class="flex justify-between gap-3">
              <button onclick="my_modal_2.showModal()" id="btn-share" data-id="${
                item.pinjaman_id
              }" class="btn btn-outline w-2/4 border-transparent text-primary rounded-full hover:bg-transparent hover:text-primary hover:border-primary hover:opacity-80 capitalize">Bagikan <i class="fa-sharp fa-regular fa-share-nodes"></i></button>
              <a href="${item.prospektus}" target="_blank" class="btn bg-primary w-2/4 text-white rounded-full hover:bg-primary hover:text-white hover:opacity-80 capitalize">Investasi</a>
            </div>
          </div>
        </div>
        </li>
        `;
      });
    } else {
      document.querySelector("#title-pilihan-proyek").innerHTML = "";
      document.querySelectorAll("[data-id='invest-footer']").forEach((element) => {
        element.remove();
      });
      document.querySelector("#testimony__section").remove();
    }
    listProject.innerHTML = list_data;
    slideProject.refresh();
  }).catch((error) => {
    console.log(error);
  });
}
logProject();

// btn share in list project
$(document).on("click", "#btn-share", function () {
  let id = $(this).data("id");

  // set title modal share
  $("#title-modal-share").text(id);

  // set into href sosmed
  $("#share-linkedin").attr("href", `https://www.linkedin.com/shareArticle?mini=true&url=${base.url()}pemodal/prospektus-views.html?prospektus=${id}`);
  $("#share-facebook").attr("href", `https://www.facebook.com/share.php?u=${base.url()}pemodal/prospektus-views.html?prospektus=${id}`);
  $("#share-twitter").attr("href", `https://twitter.com/intent/tweet?url=${base.url()}pemodal/prospektus-views.html?prospektus=${id}`);

  // btn copy share
  $("#btn-copy-share").attr("data-link-share", `${base.url()}pemodal/prospektus-views.html?prospektus=${id}`);
});

// btn event click copy share
document.querySelector("#btn-copy-share").addEventListener("click", function () {
  let link = this.getAttribute("data-link-share");
  copyToClipboard(link);
  this.innerHTML = `Link tersalin <i class="fa-solid fa-check"></i>`;
  setTimeout(() => {
    this.innerHTML = `Salin Link <i class="fa-solid fa-copy"></i>`;
  }, 2000);
});

// Scroll Animation AOS
AOS.init({
  startEvent: "DOMContentLoaded",
});

// Initialization Chart
let options = {
  series: [
    {
      name: "",
      data: [],
    },
  ],
  chart: {
    height: 350,
    type: "area",
    fontFamily: "Montserrat, sans-serif",
    toolbar: {
      show: false,
    },
    animations: {
      enabled: true,
      dynamicAnimation: {
        enabled: true,
        speed: 2000,
      },
    },
    events: {
      updated: function (a, b) {
        document.querySelector("#period-value").textContent = parseInt(b.globals.categoryLabels[0]) + " Tahun";
        document.querySelector("#potent-value").textContent = "Rp" + parseInt(b.globals.stackedSeriesTotals[0]).toLocaleString("id", "ID");
      },
    },
  },
  dataLabels: {
    enabled: false,
  },
  stroke: {
    curve: "straight",
    width: 4,
  },
  colors: ["#04af93"],
  fill: {
    gradient: {
      enabled: true,
      opacityFrom: 0.8,
      opacityTo: 0,
    },
  },
  markers: {
    size: 5,
    intersect: true,
    hover: {
      size: 9,
    },
  },
  yaxis: {
    title: {
      text: "Potensi Keuntungan",
    },
    labels: {
      formatter: function (value) {
        // return "Rp" + parseInt(value).toLocaleString("id", "ID");
        if (value >= 1000000000) {
          return (value / 1000000000).toFixed(0) + "m";
        } else if (value >= 1000000) {
          return (value / 1000000).toFixed(0) + "jt";
        } else if (value >= 1000) {
          return (value / 1000).toFixed(0) + "rb";
        } else {
          return value;
        }
      },
      style: {
        fontWeight: "600",
        colors: "#04af93",
      },
    },
  },
  xaxis: {
    type: "category",
    categories: [],
    title: {
      text: "Jangka Waktu",
    },
    labels: {
      style: {
        // fontWeight: "600",
      },
    },
    tooltip: {
      enabled: false,
    },
  },
  grid: {
    show: true,
    borderColor: "#eaeaea9b",
    strokeDashArray: 8,
  },
  tooltip: {
    custom: function ({ series, seriesIndex, dataPointIndex, w }) {
      var series = w.globals.initialSeries[seriesIndex].data[dataPointIndex];
      var categories = w.globals.categoryLabels[dataPointIndex];

      document.querySelector("#period-value").textContent = parseInt(categories) + " Tahun";
      document.querySelector("#potent-value").textContent = "Rp" + parseInt(series).toLocaleString("id", "ID");

      return "";
      // return "<ul style='font-size:12px; padding:0.1rem 0.5rem; background-color:#ffffffae'>" + "<li><b>Rp" + parseInt(series).toLocaleString("id", "ID") + "</b></li>" + "</ul>";
    },
    theme: true,
    x: {
      show: true,
    },
    y: {
      formatter: function (value) {
        return "Rp" + parseInt(value).toLocaleString("id", "ID");
      },
    },
  },
};
let chart = new ApexCharts(document.querySelector("#chart"), options);
chart.render();

// cta after footer
document.querySelectorAll("[data-id='invest-footer']").forEach((element) => {
  element.addEventListener("click", function () {
    scrollToElement("#title-pilihan-proyek");
  });
});

// ========================== END::OPTIONS ==========================

//

//

// ========================== START::DOM ==========================

// estimator
let investmentAmountAll = document.querySelectorAll('[name="investment_amount"]');
let investShow = document.querySelector("#invest-show");
let timePeriodAll = document.querySelectorAll('[name="time_period"]');
let submitEstimator = document.querySelector("#submit-estimator");
let estimatorWrapper = document.querySelector("#estimator-wrapper");
let rangeAmount = document.querySelector("#range_amount");

// validasi
const validate = () => {
  if (document.querySelector('[name="time_period"]:checked')) {
    // backup: document.querySelector('[name="investment_amount"]:checked') && document.querySelector('[name="time_period"]:checked')
    submitEstimator.disabled = false;
  } else {
    return false;
  }
  return true;
};

// investmentAmountAll.forEach((element) => {
//   element.addEventListener("change", function () {
//     if (this.checked) {
//       investShow.textContent = "Rp" + parseInt(this.value).toLocaleString("id", "ID");
//       validate();
//     }
//   });
// });

rangeAmount.addEventListener("input", function () {
  investShow.textContent = "Rp" + parseInt(this.value).toLocaleString("id", "ID");
  validate();
});

timePeriodAll.forEach((element) => {
  element.addEventListener("change", function () {
    if (this.checked) {
      validate();
    }
  });
});

// on submit estimator
submitEstimator.addEventListener("click", function () {
  // let investmentAmountChecked = document.querySelector('[name="investment_amount"]:checked').value;
  // investmentAmountChecked = parseInt(investmentAmountChecked);
  let rangeAmount = document.querySelector("#range_amount").value;
  rangeAmount = parseInt(rangeAmount);
  let timePeriodChecked = document.querySelector('[name="time_period"]:checked').value;
  timePeriodChecked = parseInt(timePeriodChecked);

  let potencyPercent = document.querySelector("#potency_percent");
  if (timePeriodChecked === 3) {
    potencyPercent.textContent = "15%";
  } else if (timePeriodChecked === 6) {
    potencyPercent.textContent = "16%";
  } else if (timePeriodChecked === 12) {
    potencyPercent.textContent = "17%";
  }

  if (!validate()) {
    return false;
  }

  estimatorWrapper.classList.remove("hidden");
  estimator(rangeAmount, timePeriodChecked);
  scrollToElement("#submit-estimator", 0);
});

const estimator = (amount, period) => {
  let imbal_hasil = 15 / 100; // 15%

  //

  // ---- RESULT IMBAL HASIL ----
  let imbalHasilPerbulan = document.querySelector("#imbal-hasil-perbulan");
  let totalImbalHasil = document.querySelector("#total-imbal-hasil");
  let totalModalImbalHasil = document.querySelector("#total-modal-imbal-hasil");

  // imbal hasil pebulan
  let imbal_hasil_perbulan = (amount * imbal_hasil) / 12;
  IncrementCountAnimate(imbalHasilPerbulan, imbal_hasil_perbulan);

  // total imbal hasil
  let total_imbal_hasil = ((amount * imbal_hasil) / 12) * period;
  IncrementCountAnimate(totalImbalHasil, total_imbal_hasil);

  // total modal & imbal hasil
  let total_modal_imbal_hasil = amount + ((amount * imbal_hasil) / 12) * period;
  IncrementCountAnimate(totalModalImbalHasil, total_modal_imbal_hasil);

  //

  // ---- BAR CHART PERBANDINGAN ----

  // EBU Danamart
  let ebuAmount = document.querySelector("#ebu-amount");
  let ebuPercent = document.querySelector("#ebu-percent");
  let ebuBar = document.querySelector("#ebu-bar");

  let ebu_amount = amount + amount * imbal_hasil;
  IncrementCountAnimate(ebuAmount, ebu_amount);

  let ebu_percent = ((ebu_amount - amount) / amount) * 100 + "%";
  ebuPercent.textContent = ebu_percent;
  ebuBar.style.width = ebu_percent;

  // Deposito
  let depositoAmount = document.querySelector("#deposito-amount");
  let depositoPercent = document.querySelector("#deposito-percent");
  let depositoBar = document.querySelector("#deposito-bar");

  let deposito_amount = amount + (amount * 4.97) / 100;
  IncrementCountAnimate(depositoAmount, deposito_amount);

  let deposito_percent = ((deposito_amount - amount) / amount) * 100 + "%";
  depositoPercent.textContent = deposito_percent;
  depositoBar.style.width = deposito_percent;

  // Reksadana
  let reksadanaAmount = document.querySelector("#reksadana-amount");
  let reksadanaPercent = document.querySelector("#reksadana-percent");
  let reksadanaBar = document.querySelector("#reksadana-bar");
  let reksadanaBarMinus = document.querySelector("#reksadana-bar-minus");
  let reksadanaIcon = document.querySelector("#reksadana-icon");

  let reksadana_amount = amount + (amount * -1.2) / 100;
  IncrementCountAnimate(reksadanaAmount, reksadana_amount);

  let reksadana_percent = ((reksadana_amount - amount) / amount) * 100;

  reksadanaPercent.textContent = reksadana_percent + "%";

  if (reksadana_percent >= 0) {
    reksadanaBar.style.width = reksadana_percent + "%";
  } else {
    reksadanaIcon.classList.replace("fa-caret-up", "fa-caret-down");
    reksadanaBarMinus.style.width = Math.abs(reksadana_percent) + "%";
  }

  // Saham
  let sahamAmount = document.querySelector("#saham-amount");
  let sahamPercent = document.querySelector("#saham-percent");
  let sahamBar = document.querySelector("#saham-bar");
  let sahamBarMinus = document.querySelector("#saham-bar-minus");
  let sahamIcon = document.querySelector("#saham-icon");

  let saham_amount = amount + (amount * -3.12) / 100;
  IncrementCountAnimate(sahamAmount, saham_amount);

  let saham_percent = ((saham_amount - amount) / amount) * 100;

  sahamPercent.textContent = saham_percent.toFixed(2) + "%";

  if (saham_percent >= 0) {
    sahamBar.style.width = saham_percent + "%";
  } else {
    sahamIcon.classList.replace("fa-caret-up", "fa-caret-down");
    sahamBarMinus.style.width = Math.abs(saham_percent) + "%";
  }

  // Obligasi
  let obligasiAmount = document.querySelector("#obligasi-amount");
  let obligasiPercent = document.querySelector("#obligasi-percent");
  let obligasiBar = document.querySelector("#obligasi-bar");

  let obligasi_amount = amount + (amount * 4.95) / 100;
  IncrementCountAnimate(obligasiAmount, obligasi_amount);

  let obligasi_percent = ((obligasi_amount - amount) / amount) * 100 + "%";
  obligasiPercent.textContent = obligasi_percent;
  obligasiBar.style.width = obligasi_percent;

  // Emas
  let emasAmount = document.querySelector("#emas-amount");
  let emasPercent = document.querySelector("#emas-percent");
  let emasBar = document.querySelector("#emas-bar");

  let emas_amount = amount + (amount * 11.72) / 100;
  IncrementCountAnimate(emasAmount, emas_amount);

  let emas_percent = ((emas_amount - amount) / amount) * 100 + "%";
  emasPercent.textContent = emas_percent;
  emasBar.style.width = emas_percent;

  //

  // ---- POTENSI KEUNTUNGAN HASIL ----

  // Table Basic
  let tablePotensi = document.querySelector("#table-potensi");

  let years1 = amount * Math.pow(1 + 0.15 / 12, 12 * 1);
  years1 = years1.toFixed(0).toLocaleString("id", "ID");

  let years5 = amount * Math.pow(1 + 0.15 / 12, 12 * 5);
  years5 = years5.toFixed(0).toLocaleString("id", "ID");

  let years10 = amount * Math.pow(1 + 0.15 / 12, 12 * 10);
  years10 = years10.toFixed(0).toLocaleString("id", "ID");

  let years20 = amount * Math.pow(1 + 0.15 / 12, 12 * 20);
  years20 = years20.toFixed(0).toLocaleString("id", "ID");

  let years30 = amount * Math.pow(1 + 0.15 / 12, 12 * 30);
  years30 = years30.toFixed(0).toLocaleString("id", "ID");

  let arrayPotent = [
    {
      years: "1 Tahun",
      result: years1,
    },
    {
      years: "5 Tahun",
      result: years5,
    },
    {
      years: "10 Tahun",
      result: years10,
    },
    {
      years: "20 Tahun",
      result: years20,
    },
    {
      years: "30 Tahun",
      result: years30,
    },
  ];

  let resultPotensi = "";
  arrayPotent.forEach((item) => {
    resultPotensi += `
    <tr>
      <td>${item.years}</td>
      <td style="font-weight:600">Rp ${parseInt(item.result).toLocaleString("id", "ID")}</td>
    </tr>
    `;
  });
  tablePotensi.innerHTML = resultPotensi;

  // Update Chart
  function updateChart(newData, newCategories) {
    // Update data seri
    chart.updateSeries([{ data: newData }]);

    // Update label sumbu x
    chart.updateOptions({
      xaxis: {
        categories: newCategories,
      },
    });
  }

  updateChart(
    arrayPotent.map((item) => item.result),
    arrayPotent.map((item) => item.years)
  );
};

// Disclaimer toogler
let btnToggleDisclaimer = document.querySelector("#toggle-disclaimer");
btnToggleDisclaimer.addEventListener("click", function () {
  let moreDisclaimer = document.querySelector("#more-disclaimer");
  moreDisclaimer.classList.toggle("hidden");
  let moreDisclaimerHidden = document.querySelector("#more-disclaimer.hidden") ? true : false;
  if (moreDisclaimerHidden) {
    btnToggleDisclaimer.textContent = "Baca lebih lanjut";
  } else {
    btnToggleDisclaimer.textContent = "Tutup";
  }
});

// ========================== END::DOM ==========================

//
