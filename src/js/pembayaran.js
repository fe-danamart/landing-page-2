require("../index");

const qrcode = require("qrcode");
import { Crypto, toLocaleIndoTime, base } from "./custom-tools";
import html2canvas from "html2canvas";

// Fungsi untuk mengunduh gambar dari tag <img>
function downloadImageFromImgTag() {
  const imgElement = document.querySelector("#canvas");

  html2canvas(imgElement).then((canvas) => {
    // Buat tautan download
    const link = document.createElement("a");
    link.href = canvas.toDataURL();
    link.download = "qris-danamart.png";
    link.click();
  });
}
document.getElementById("download-qris").addEventListener("click", downloadImageFromImgTag);

//

const url = new URLSearchParams(new URL(window.location.href).search);
const pay = url.get("pay");

// check already parameter
if (!pay) {
  history.back();
}

// countdown expired
const countdownExpired = (targetDate = null, idSelector = null) => {
  // Waktu target dalam format "YYYY-MM-DD, HH:MM:SS"
  const targetTime = new Date(targetDate).getTime();

  // Fungsi untuk mengupdate countdown setiap detik
  const updateCountdown = setInterval(() => {
    const now = new Date().getTime();
    const distance = targetTime - now;

    if (distance < 0) {
      clearInterval(updateCountdown);
      document.querySelector(idSelector).innerHTML = '<span class="text-danger">Waktu Habis</span>';
      document.querySelector("#canvas img").remove();
      document.querySelector("#canvas button").remove();
    } else {
      const days = Math.floor(distance / (1000 * 60 * 60 * 24));
      const hours = String(Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))).padStart(2, "0");
      const minutes = String(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))).padStart(2, "0");
      const seconds = String(Math.floor((distance % (1000 * 60)) / 1000)).padStart(2, "0");
      document.querySelector(idSelector).innerHTML = `Kode Berlaku <span class="color-red-dark">${hours}:${minutes}:${seconds}</span>`;
    }
  }, 1000);
};

// call data
const settings = {
  url: base.endpoint() + "pemodal/Pendanaan/getPembayaranGuest?orderId=" + pay,
  method: "GET",
  timeout: 0,
};

const callData = new Promise(function (resolve, reject) {
  return $.ajax(settings).done(function (res) {
    let response = Crypto.decryptResponse(res);
    if (response.Data) {
      resolve(response);
    }
  });
});

// first
callData.then(function (res) {
  let data = res.Data;
  let { kode_pembayaran, expiration_date, jml_pendanaan, fva_id, status_payment } = data;
  document.querySelector("#expired-time").textContent = toLocaleIndoTime(expiration_date) + " WIB";
  // document.querySelector("#order_id").textContent = fva_id;
  document.querySelector("#nominal").textContent = "Rp" + parseInt(jml_pendanaan).toLocaleString("id", "ID") + ",-";
  countdownExpired(expiration_date, "#countdown");

  statusPayment(status_payment, res.tknId);

  // Opsi QR Code (opsional)
  const options = {
    type: "image/png",
    quality: 0.92,
    margin: 1,
    color: {
      dark: "#000", // Warna elemen QR Code (hitam)
      light: "#fff", // Warna background (putih)
    },
  };

  // Fungsi untuk membuat QR Code
  const createQRCode = async (kode_pembayaran) => {
    try {
      // Generate QR Code sebagai Base64 data URL
      const qrCodeData = await qrcode.toDataURL(kode_pembayaran, options);

      // Output hasil QR Code sebagai gambar
      document.querySelector("#canvas img").src = qrCodeData;
    } catch (err) {
      console.error(err);
    }
  };

  // Panggil fungsi untuk membuat QR Code
  createQRCode(kode_pembayaran);
});

const statusPayment = (status_payment = null, tknId = null) => {
  let info_aktif = document.querySelector("#info-aktif"),
    panduan_pembayaran = document.querySelector("#panduan-pembayaran");

  if (status_payment === "BELUM BAYAR") {
    status_payment = `<span class="font-semibold">${status_payment}</span>`;
  } else if (status_payment === "SUDAH BAYAR") {
    status_payment = `<span class="font-semibold">${status_payment}</span>`;
    info_aktif.classList.add("hidden");
    panduan_pembayaran.classList.add("hidden");
    my_modal_1.showModal();
    document.querySelector("#redirect_login").addEventListener("click", function () {
      (window.location.href = base.url() + "?kode=" + tknId), "_blank";
    });
  } else if (status_payment === "TRANSAKSI DIHAPUS") {
    status_payment = `<span class="font-semibold">DIBATALKAN</span>`;
    info_aktif.classList.add("hidden");
    panduan_pembayaran.classList.add("hidden");
  }
  document.querySelector("#status").classList.remove("hidden");
  document.querySelector("#status").innerHTML = status_payment;
};

// loop
setInterval(() => {
  $.ajax(settings).done(function (res) {
    let response = Crypto.decryptResponse(res);
    let { status_payment } = response.Data;
    statusPayment(status_payment, response.tknId);
  });
}, 3000);
