require("../index");

import { base } from "./custom-tools";

import Cleave from "../vendor/cleave/dist/cleave-esm.js";
require("../vendor/cleave/dist/addons/cleave-phone.in.js");
require("./cleave-options.js");

const url = new URLSearchParams(new URL(window.location.href).search);
const offer = url.get("offer");

let formRegister = document.querySelector("#form-register");
let modalPilihMetode = document.querySelector("#modal-pilih-metode");
let modalBtnClose = document.querySelector("#modal-btn-close");
let investmentAmountAll = document.querySelectorAll('[name="investment_amount"]');
let investmentAmountOther = document.querySelector('[name="investment_amount_other"]');
let coldplayTicket = document.querySelector("#coldplay-ticket");

// START:: get data by id
const getProjectById = new Promise(async (resolve, reject) => {
  try {
    const response = await fetch(base.endpoint() + "pemodal/Prospektus/guest?pinjamanId=" + offer, {
      method: "GET",
      headers: { "Content-type": "application/json;charset=UTF-8" },
    });
    const data = await response.json();

    if (data.Data) {
      document.querySelector("#project-name").textContent = data.Data.NamaPenerbit;
    } else {
      modal_alert.querySelector("#modal_alert_title").innerHTML = '<i class="fa-sharp fa-regular fa-circle-exclamation"></i> Proyek tidak ditemukan!</b>';
      modal_alert.showModal();
      setTimeout(() => {
        window.location.href = "./";
      }, 2000);
      return false;
    }

    // START:: get data form by id & user
    try {
      const response2 = await fetch(base.endpoint() + `pemodal/Pendanaan/formGuest/${data.Data.BeliEfek.PinjamanId}/${data.Data.BeliEfek.UserPinjamanId}`, {
        method: "GET",
        headers: { "Content-type": "application/json;charset=UTF-8" },
      });
      const data2 = await response2.json();

      let { dataForm, dataInput } = data2.data;

      let slot_pembelian = parseInt(dataForm.slotPembelian.replaceAll(".", ""));

      document.querySelector("#slot-pembelian").innerHTML = "Rp" + slot_pembelian.toLocaleString("id", "ID");

      resolve([slot_pembelian, dataInput]);
    } catch (error) {
      console.log(error);
    }
    // END:: get data form by id user
  } catch (error) {
    reject(error);
  }
});
// END:: get data by id

investmentAmountAll.forEach((item) => {
  item.addEventListener("change", function () {
    investmentAmountOther.value = "";

    let ticketAmount = parseInt(this.value) / 100000;
    coldplayTicket.classList.remove("hidden");
    coldplayTicket.querySelector("#coldplay-ticket-counter").textContent = ticketAmount;
  });
});

investmentAmountOther.addEventListener("input", function () {
  investmentAmountAll.forEach((item) => {
    item.checked = false;
  });

  let this_value = parseInt(this.value.replaceAll(".", ""));
  let ticketAmount = this_value / 100000;
  ticketAmount = parseInt(ticketAmount.toString().replace(/(\.\d+)/, ""));

  if (this_value >= 100000 && ticketAmount >= 1) {
    coldplayTicket.classList.remove("hidden");
    coldplayTicket.querySelector("#coldplay-ticket-counter").textContent = ticketAmount;
  } else {
    coldplayTicket.classList.add("hidden");
  }
});

const form_register = (form) => {
  let full_name = form.querySelector('[name="full_name"]');
  let phone_number = form.querySelector('[name="phone_number"]');
  let email = form.querySelector('[name="email"]');
  let investment_amount_checked = form.querySelector('[name="investment_amount"]:checked');

  // validasi
  if (!full_name.value || !phone_number.value || !email.value) {
    console.log("tidak lengkap");
    return false;
  }

  let invetmentAmountChecked;
  if (investment_amount_checked) {
    invetmentAmountChecked = parseInt(investment_amount_checked.value);
  } else if (investmentAmountOther.value) {
    invetmentAmountChecked = parseInt(investmentAmountOther.value.replaceAll(".", ""));
  } else {
    modal_alert.querySelector("#modal_alert_title").innerHTML = '<i class="fa-sharp fa-regular fa-circle-exclamation"></i> Pilih Jumlah Investasi</b>';
    modal_alert.showModal();
    window.scrollTo(0, 180);
    return false;
  }

  if (invetmentAmountChecked < 100000) {
    modal_alert.querySelector("#modal_alert_title").innerHTML = '<i class="fa-sharp fa-regular fa-circle-exclamation"></i> Minimal Investasi <b>Rp100.000</b>';
    modal_alert.showModal();
    return false;
  } else if (invetmentAmountChecked >= 1000000) {
    modalPilihMetode.classList.remove("hidden");
  } else {
    post_data(invetmentAmountChecked, full_name.value, email.value, phone_number.value, null);
  }
};

formRegister.addEventListener("submit", function () {
  form_register(this);
});

// close payment method
modalBtnClose.addEventListener("click", function () {
  modalPilihMetode.classList.add("hidden");
});

// pilih metode pembayaran
let payment_method = document.querySelectorAll("[payment-method]");

payment_method.forEach((element) => {
  element.addEventListener("click", function () {
    let paymentMethod = this.getAttribute("payment-method");
    let full_name = formRegister.querySelector('[name="full_name"]');
    let phone_number = formRegister.querySelector('[name="phone_number"]');
    let email = formRegister.querySelector('[name="email"]');
    let investment_amount_checked = formRegister.querySelector('[name="investment_amount"]:checked');

    // validasi
    if (!full_name.value || !phone_number.value || !email.value) {
      console.log("tidak lengkap");
      return false;
    }

    let invetmentAmountChecked;
    if (investment_amount_checked) {
      invetmentAmountChecked = parseInt(investment_amount_checked.value);
    } else if (investmentAmountOther.value) {
      invetmentAmountChecked = parseInt(investmentAmountOther.value.replaceAll(".", ""));
    } else {
      modal_alert.querySelector("#modal_alert_title").innerHTML = '<i class="fa-sharp fa-regular fa-circle-exclamation"></i> Pilih Jumlah Investasi</b>';
      modal_alert.showModal();
      window.scrollTo(0, 180);
      return false;
    }

    if (invetmentAmountChecked < 100000) {
      modal_alert.querySelector("#modal_alert_title").innerHTML = '<i class="fa-sharp fa-regular fa-circle-exclamation"></i> Minimal Investasi <b>Rp100.000</b>';
      modal_alert.showModal();
      return false;
    } else {
      post_data(invetmentAmountChecked, full_name.value, email.value, phone_number.value, paymentMethod);
    }
  });
});

const post_data = (investment_amount_checked, full_name, email, phone_number, paymentMethod = null) => {
  getProjectById.then((data) => {
    let slotPembelian = data[0];
    let dataInput = data[1];

    // validate
    if (investment_amount_checked > slotPembelian) {
      modal_alert.querySelector("#modal_alert_title").innerHTML = '<i class="fa-sharp fa-regular fa-circle-exclamation"></i> Jumlah Investasi Melebihi Penawaran Tersedia';
      modal_alert.showModal();
      console.log("melebihi slot");
      return false;
    }

    let { bunga_persen, credit_rating, dm_pem_02003, dm_pem_02004, jml_pinjaman_terbit, pinjaman_id, referral_id_lv1_peminjam, referral_id_lv1_pendana, referral_id_lv2_peminjam, referral_id_lv2_pendana, sektor_usaha, tgl_jatuh_tempo, total_dana_reward, user_peminjam_id } = dataInput;

    phone_number = phone_number.replace(/^(\+?62|0)/, "62"); //always 62xx
    phone_number = phone_number.replace(/^\+?/, ""); //remove +

    let formAppend = new FormData();
    formAppend.append("bid", investment_amount_checked);
    formAppend.append("nama", full_name);
    formAppend.append("email", email);
    formAppend.append("nohp", phone_number);
    formAppend.append("bank", paymentMethod);

    // form api
    formAppend.append("user_peminjam_id", user_peminjam_id);
    formAppend.append("user_pendana_id", null);
    formAppend.append("pinjaman_id", pinjaman_id);
    formAppend.append("sektor_usaha", sektor_usaha);
    formAppend.append("bunga_persen", bunga_persen);
    formAppend.append("credit_rating", credit_rating);
    formAppend.append("dm_pem_02003", dm_pem_02003);
    formAppend.append("dm_pem_02004", dm_pem_02004);
    formAppend.append("jml_pinjaman_terbit", jml_pinjaman_terbit);
    formAppend.append("tgl_jatuh_tempo", tgl_jatuh_tempo);
    formAppend.append("referral_id_lv1_peminjam", referral_id_lv1_peminjam);
    formAppend.append("referral_id_lv2_peminjam", referral_id_lv2_peminjam);
    formAppend.append("referral_id_lv1_pendana", referral_id_lv1_pendana);
    formAppend.append("referral_id_lv2_pendana", referral_id_lv2_pendana);
    formAppend.append("total_dana_reward", total_dana_reward);

    $.ajax({
      url: base.endpoint() + "Daftar/Email/deposit?type=Beli",
      method: "POST",
      timeout: 0,
      processData: false,
      mimeType: "multipart/form-data",
      contentType: false,
      data: formAppend,
      beforeSend: function () {
        $("body").prepend(`
          <div id="fetch-loader">
            <img src="./static/danamart.png" alt="danamart.png" class="animate-bounce object-contain" style="width: 2.75rem" />
          </div>
          `);
      },
      complete: function () {
        $("#fetch-loader").remove();
      },
    })
      .done(function (res) {
        let response = JSON.parse(res);
        let { status_transaksi, status_akun, transaksi } = response;

        console.log(response);

        if ((status_akun === "new" && status_transaksi == false) || (status_akun === "registered" && status_transaksi == false)) {
          // akun baru || akun terdaftar & belum ada transaksi
          if (transaksi === "va") {
            window.location.href = response.hitResponse.response_yukk.result.redirect_url;
          } else if (transaksi === "qriss") {
            window.location.href = "pembayaran.html?pay=" + response.hitResponse.order;
          }
        } else if (status_akun === "registered" && status_transaksi == true) {
          // akun terdaftar & sudah ada transaksi
          modal_alert.querySelector("#modal_alert_title").innerHTML = '<i class="fa-sharp fa-regular fa-circle-exclamation"></i> Akun sudah terdaftar dan ada transaksi yang belum dibayarkan. <b><a href="https://scf.danamart.id/webapp/">Masuk</a></b>';
          modal_alert.showModal();
          return false;
        }
      })
      .fail(function (options) {
        // pesan error tanpa dikasih info login (hanya ganti dengan yang unix)
        if (options.status === 500) {
          let { message } = JSON.parse(options.responseText);
          if (message === "Undefined index: account_number") {
            modal_alert.querySelector("#modal_alert_title").innerHTML = '<i class="fa-sharp fa-regular fa-circle-exclamation"></i> Silakan coba lagi. Gunakan nama, email atau nomor yang berbeda.';
            modal_alert.showModal();
            return false;
          } else {
            console.log(message);
          }
        } else {
          // tampilkan pesan error dan link redirect login
          let { messages } = JSON.parse(options.responseText);
          if (messages.email) {
            modal_alert.querySelector("#modal_alert_title").innerHTML = '<i class="fa-sharp fa-regular fa-circle-exclamation"></i> Email sudah terdaftar silakan gunakan email lain atau <b><a href="https://scf.danamart.id/webapp/">Masuk</a></b>';
            modal_alert.showModal();
            return false;
          } else if (messages.nohp) {
            modal_alert.querySelector("#modal_alert_title").innerHTML = '<i class="fa-sharp fa-regular fa-circle-exclamation"></i> No Handphone sudah terdaftar silakan gunakan nomor lain atau <b><a href="https://scf.danamart.id/webapp/">Masuk</a></b>';
            modal_alert.showModal();
          }
        }
      });
  });
};
