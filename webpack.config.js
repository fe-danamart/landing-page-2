const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");

module.exports = {
  mode: "development",
  entry: {
    // netcore: { import: "./src/js/netcore.js", filename: "[name].js" },

    home: "./src/js/home.js",
    register: "./src/js/register.js",
    pembayaran: "./src/js/pembayaran.js",
  },
  output: {
    path: path.resolve(__dirname, "build"),
    // path: path.resolve(__dirname, "../landing-page-productions"),
    filename: "[name].[contenthash].js",
    clean: true,
    assetModuleFilename: "static/[name][ext]",
  },
  devServer: {
    static: {
      directory: path.resolve(__dirname, "build"),
      // directory: path.resolve(__dirname, "../landing-page-productions"),
    },
    port: 3000,
    open: false,
    hot: true,
    compress: true,
    historyApiFallback: {
      rewrites: [
        { from: /^\/register/, to: "/register.html" },
        { from: /^\/investasi-urun-dana-potensi-untung-maksimal/, to: "/index.html" },
      ],
    },
  },
  module: {
    rules: [
      {
        test: /\.(scss|css|sass)$/i,
        include: path.resolve(__dirname, "src"),
        use: ["style-loader", "css-loader", "sass-loader", "postcss-loader"],
      },
      {
        test: /\.(?:js|mjs|cjs)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [["@babel/preset-env", { targets: "defaults" }]],
          },
        },
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif|ico|webm)$/i,
        type: "asset/resource",
      },
    ],
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
    }),

    // homepage
    new HtmlWebpackPlugin({
      title: "Yuk Invest! x Danamart",
      filename: "index.html",
      inject: true,
      template: "src/pages/index.html",
      chunks: ["home"],
      // meta: {
      //   "http-equiv": {
      //     "http-equiv": "refresh",
      //     content: "2; url=investasi-urun-dana-potensi-untung-maksimal.html",
      //   },
      // },
    }),
    // custom url
    // new HtmlWebpackPlugin({
    //   title: "Investasi Urun Dana Potensi Untung Maksimal",
    //   filename: "investasi-urun-dana-potensi-untung-maksimal.html",
    //   inject: true,
    //   template: "src/pages/index.html",
    //   chunks: ["home"],
    // }),
    // register
    new HtmlWebpackPlugin({
      title: "Daftar Sekarang",
      filename: "register.html",
      inject: true,
      template: "src/pages/register.html",
      chunks: ["register"],
    }),
    // pembayaran
    new HtmlWebpackPlugin({
      title: "Pembayaran",
      filename: "pembayaran.html",
      inject: true,
      template: "src/pages/pembayaran.html",
      chunks: ["pembayaran"],
    }),
  ],
};
