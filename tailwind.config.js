module.exports = {
  content: ["./dist/*.html", "./src/pages/*.html"],
  theme: {
    extend: {
      colors: {
        primary: "#04af93",
        secondary: "#1f204e",
      },
      fontSize: {
        mxs: "0.7rem",
        xxs: "0.6rem",
      },
    },
  },
  variants: {
    extend: {},
  },
  daisyui: {
    themes: ["light"],
  },
  plugins: [require("daisyui")],
};
